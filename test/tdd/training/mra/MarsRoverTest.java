package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void testMarsRoverConstructorXDimension() throws MarsRoverException {
		List<String> obstacles = new ArrayList<String>();
		MarsRover newPlanet = new MarsRover(10,10,obstacles);
		assertEquals(10, newPlanet.planetX);
	}
	@Test
	public void testMarsRoverConstructorYDimension() throws MarsRoverException {
		List<String> obstacles = new ArrayList<String>();
		MarsRover newPlanet = new MarsRover(10,10,obstacles);
		assertEquals(10, newPlanet.planetY);
	}
	@Test
	public void testPlanetContainsObstacleAt() throws MarsRoverException {
		List<String> obstacles = new ArrayList<String>();
		obstacles.add("(4,7)");
		obstacles.add("(2,3)");
		MarsRover newPlanet = new MarsRover(10,10,obstacles);
		assertTrue(newPlanet.planetContainsObstacleAt(4,7));
	}
	@Test
	public void testMarsRoverDirectionShouldBeNorth() throws MarsRoverException {
		List<String> obstacles = new ArrayList<String>();
		MarsRover newPlanet = new MarsRover(10,10,obstacles);
		assertEquals("N", newPlanet.direction);
	}
	@Test
	public void testExecuteCommandWithEmptyString() throws MarsRoverException {
		List<String> obstacles = new ArrayList<String>();
		MarsRover newPlanet = new MarsRover(10,10,obstacles);
		assertEquals("(0,0,N)", newPlanet.executeCommand(""));
	}
	@Test
	public void testExecuteCommandTurningRight() throws MarsRoverException {
		List<String> obstacles = new ArrayList<String>();
		MarsRover newPlanet = new MarsRover(10,10,obstacles);
		assertEquals("(0,0,E)", newPlanet.executeCommand("r"));
	}
	@Test
	public void testExecuteCommandTurningLeft() throws MarsRoverException {
		List<String> obstacles = new ArrayList<String>();
		MarsRover newPlanet = new MarsRover(10,10,obstacles);
		assertEquals("(0,0,W)", newPlanet.executeCommand("l"));
	}
	@Test
	public void testExecuteCommandTurningLeftShouldReturnS() throws MarsRoverException {
		List<String> obstacles = new ArrayList<String>();
		MarsRover newPlanet = new MarsRover(10,10,obstacles);
		newPlanet.executeCommand("l");
		assertEquals("(0,0,S)", newPlanet.executeCommand("l"));
	}	
	@Test
	public void testExecuteCommandMovingForward() throws MarsRoverException {
		List<String> obstacles = new ArrayList<String>();
		MarsRover newPlanet = new MarsRover(10,10,obstacles);
		assertEquals("(0,1,N)", newPlanet.executeCommand("f"));
	}
	@Test
	public void testExecuteCommandMovingBackward() throws MarsRoverException {
		List<String> obstacles = new ArrayList<String>();
		MarsRover newPlanet = new MarsRover(10,10,obstacles);
		newPlanet.executeCommand("f");
		newPlanet.executeCommand("f");
		assertEquals("(0,1,N)", newPlanet.executeCommand("b"));
	}
	@Test
	public void testExecuteCommandMovingBackwardFacingEast() throws MarsRoverException {
		List<String> obstacles = new ArrayList<String>();
		MarsRover newPlanet = new MarsRover(10,10,obstacles);
		newPlanet.executeCommand("r");
		newPlanet.executeCommand("f");
		newPlanet.executeCommand("f");
		assertEquals("(1,0,E)", newPlanet.executeCommand("b"));
	}
	@Test
	public void testExecuteCombinationOfMultipleCommands() throws MarsRoverException {
		List<String> obstacles = new ArrayList<String>();
		MarsRover newPlanet = new MarsRover(10,10,obstacles);
		assertEquals("(2,2,E)", newPlanet.executeCommand("ffrff"));
	}
	@Test
	public void testExecuteCommandsOverPlanetEdgeNorth() throws MarsRoverException {
		List<String> obstacles = new ArrayList<String>();
		MarsRover newPlanet = new MarsRover(10,10,obstacles);
		assertEquals("(0,9,N)", newPlanet.executeCommand("b"));
	}
	@Test
	public void testExecuteCommandsOverPlanetEdgeSouth() throws MarsRoverException {
		List<String> obstacles = new ArrayList<String>();
		MarsRover newPlanet = new MarsRover(10,10,obstacles);
		newPlanet.x=0;
		newPlanet.y=9;
		newPlanet.executeCommand("rr");
		assertEquals("(0,0,S)", newPlanet.executeCommand("b"));
	}
	@Test
	public void testExecuteCommandsEncounteringSingleObstacleTwice() throws MarsRoverException {
		List<String> obstacles = new ArrayList<String>();
		obstacles.add("(2,2)");
		MarsRover newPlanet = new MarsRover(10,10,obstacles);
		assertEquals("(1,2,E)(2,2)", newPlanet.executeCommand("ffrfff"));
	}
	@Test
	public void testExecuteCommandsEncounteringMultipleObstacles() throws MarsRoverException {
		List<String> obstacles = new ArrayList<String>();
		obstacles.add("(2,2)");
		obstacles.add("(2,1)");
		MarsRover newPlanet = new MarsRover(10,10,obstacles);
		assertEquals("(1,1,E)(2,2)(2,1)", newPlanet.executeCommand("ffrfffrflf"));
	}
	@Test
	public void testExecuteCommandsEncounteringObstacleOverTheEdge() throws MarsRoverException {
		List<String> obstacles = new ArrayList<String>();
		obstacles.add("(0,9)");
		MarsRover newPlanet = new MarsRover(10,10,obstacles);
		assertEquals("(0,0,N)(0,9)", newPlanet.executeCommand("b"));
	}
	@Test
	public void testExecuteCommandsTourAroundThePlanet() throws MarsRoverException {
		List<String> obstacles = new ArrayList<String>();
		obstacles.add("(2,2)");
		obstacles.add("(0,5)");
		obstacles.add("(5,0)");

		MarsRover newPlanet = new MarsRover(6,6,obstacles);
		assertEquals("(0,0,N)(2,2)(0,5)(5,0)", newPlanet.executeCommand("ffrfffrbbblllfrfrbbl"));
	}
}
