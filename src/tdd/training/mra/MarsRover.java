package tdd.training.mra;

import java.util.ArrayList;
import java.util.List;

public class MarsRover {

	public int planetX;
	public int planetY;
	public int x;
	public int y;
	public List<String> planetObstacles = new ArrayList<String>();
	public String direction;
	public List<String> directions = new ArrayList<String>();
	private int indexDirection;
	private int newIndex;
	private String obstaclesEncountered ="";
	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		this.planetX = planetX;
		this.planetY = planetY;
		this.planetObstacles = planetObstacles;
		direction = "N";
		x = 0;
		y = 0;
		directions.add("N");
		directions.add("E");
		directions.add("S");
		directions.add("W");
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		if(planetObstacles.contains("("+x+","+y+")")) {
			if(!obstaclesEncountered.contains("("+x+","+y+")")) {				
				obstaclesEncountered += "("+x+","+y+")";
			}
			return true;
		}
		return false;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		if(commandString == " " || commandString == "") {
			return "(0,0,N)";
		}
		String[] array = commandString.split("");
		
		for(String command : array) {
			indexDirection = directions.indexOf(direction);

			if(command.equals("r")) {
				if(indexDirection == 3) {
					newIndex = 0;
				} else {
					newIndex = directions.indexOf(direction)+1;
				}
				direction = directions.get(newIndex);
			}
			if(command.equals("l")) {
				if(indexDirection == 0) {
					newIndex = 3;
				} else {
					newIndex = (directions.indexOf(direction) ) - 1;
				}
				direction = directions.get(newIndex);
			}
			
			if(command.equals("f")) {
				moveForward();
			}
			if(command.equals("b")) {
				moveBackward();
			}
		}
		return "(" + x + "," + y +"," +direction+")" +obstaclesEncountered;
	}
	
	
	private void moveForward() throws MarsRoverException {
		
		if(direction == "N") {
			if(y==planetY-1) {
				if(!planetContainsObstacleAt(x,0)) {					
					y = 0;
				}
			} else {
				if(!planetContainsObstacleAt(x,y+1)) {
					y++;					
				}
			}
		} 
		if(direction =="S") {
			if(y==0) {
				if(!planetContainsObstacleAt(x,planetY-1)) {					
					y = planetY-1;
				} 
			} else {
				if(!planetContainsObstacleAt(x,y-1)) {					
					y--;
				}
			}
		}
		if(direction == "W") {
			if(x == 0) {
				if(!planetContainsObstacleAt(planetX-1, y)) {					
					x = planetX-1;
				}
			} else {
				if(!planetContainsObstacleAt(x-1,y)) {
					x--;					
				}
			}
		}
		if(direction == "E") {
			if(x == planetX-1) {
				if(!planetContainsObstacleAt(0, y)) {										
					x = 0;
				}
			}
			else {
				if(!planetContainsObstacleAt(x+1,y)) {			
					x++;
				}
			}
		}
	}
	
	private void moveBackward() throws MarsRoverException {
		if(direction == "N") {
			if(y == 0) {
				if(!planetContainsObstacleAt(x,planetY-1)) {
					y = planetY-1;					
				}
			} else if(!planetContainsObstacleAt(x,y-1)) {				
				y--;
			}
		}
		if(direction == "S") {
			if(y == planetY-1) {
				if(!planetContainsObstacleAt(x,0)) {					
					y = 0;
				}
			} else {
				if(!planetContainsObstacleAt(x,y-1)) {
					y++;									
				}
			}
		}
		
		if(direction == "W") {
			if(x==planetX-1) {
				if(!planetContainsObstacleAt(0,y)) {
					x = 0;					
				}
			} else if(!planetContainsObstacleAt(x+1,y)){
				x++;
			}
		}
		if(direction == "E") {
			if(x == 0) {
				if(!planetContainsObstacleAt(planetX-1,y)) {					
					x = planetX-1;
				}
			} else if(!planetContainsObstacleAt(x-1,y)){
				x--;
			}
		}
	}

}
